#  Use Cases

## An Approach In Agility

----
# Automagically created

- by Marco Feltmann
- at 2020-08-11T12:05:10Z

You might ask the authoring authority for an update if needed.

----

# Backlog

----

#  [MSCW][SP] Use Case Title

With some short description in it.
Which may not exceed four lines and 50 characters per line. 
This is especially true for some themes but KISS is key!

## 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅

----

# Options

----

#  [MSCW][SP] Use Case Title

With some short description in it.
Which may not exceed four lines and 50 characters per line. 
This is especially true for some themes but KISS is key!

## 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅

----

# InProgress

----

#  [MSCW][SP] Use Case Title

With some short description in it.
Which may not exceed four lines and 50 characters per line. 
This is especially true for some themes but KISS is key!

## 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅

----

# InTesting

----

#  [MSCW][SP] Use Case Title

With some short description in it.
Which may not exceed four lines and 50 characters per line. 
This is especially true for some themes but KISS is key!

## 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅

----

# Done

----

#  1st Done Item

According to Title: Centering

----

#  2nd Done Item

According to Title: Footing

----

#  3rd Done Item

According to Title: Heading

----

#  [MSCW][SP] Use Case Title

With some short description in it.
Which may not exceed four lines and 50 characters per line. 
This is especially true for some themes but KISS is key!

## 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅 | 🍅🍅🍅🍅

----

# That's All Folks.
## In Hamburg sagt man "Tschüss"!
