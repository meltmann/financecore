#!/bin/bash

function outputPageBreak {
	echo "" >> Combined.md 
	echo "----" >> Combined.md
	echo "" >> Combined.md 
}

function outputPresentationHeading {
	cat Heading.md > Combined.md

	echo "# Automagically created" >> Combined.md
	echo "" >> Combined.md
	echo -n "- by " >> Combined.md
	dscl . -read /Users/`whoami` RealName | sed -n 's/^ //g;2p' >> Combined.md
	echo -n "- at " >> Combined.md
	date -u +"%FT%TZ" >> Combined.md
	echo "" >> Combined.md
	echo "You might ask the authoring authority for an update if needed." >> Combined.md
	
	outputPageBreak
}

function outputPresentationFooter {
	echo "# That's All Folks." >> Combined.md
	echo "## In Hamburg sagt man \"Tschüss\"!" >> Combined.md
}

function outputUseCases {
	directories=( Backlog Options InProgress InTesting Done ) 
	for directory in "${directories[@]}"; 
		do echo "# $directory" >> Combined.md; 
	
		outputPageBreak
	
		for file in `ls ${directory}/*.md`;
			do cat "${file}" >> Combined.md;
			
			outputPageBreak
		done;
	done;
}

echo "Combining sub documents"

## Main Script Entry Point
#

outputPresentationHeading

outputUseCases

outputPresentationFooter