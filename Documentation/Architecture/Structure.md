#  Application Structure

Since every application has some kind of architecture it seems reasonable to document it.

# Highest Detail, Most Specified

## Money

The `Money` package provides the building blocks for financial work.
That is the `Money` item itself which is a compound data structure containing a `value` and a `unit`.

See [Architecture Decision Record 0001](adr/ADR0001-Money_package.md) to find out why I didn't provide some `Measure<UnitCurrency>` approach on my own.
