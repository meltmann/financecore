#  ADR0001 - Use `Money` package

## Context

`Swift` provides a `Measure<MeasureUnit>` approach to group pairs of value and unit. Since money has a value and an unit this approach seems resonable.
This approach is heavily used for physical units like `UnitMass` or `UnitSpeed`.
Since *money* is no physically relevant unit there is no pre-defined `UnitCurrency` in the system.

On the other hand is *money* something widely used in a lot of applications and therefor several approaches handling *money* exist.
The one matching the name of the concept, [Money](https://github.com/Flight-School/Money) by [Mattt Thompson](https://mat.tt), is used in a book series to teach Swift programming with `Codable` protocol. 

## Decision

I'll stick with the `Money` package from Mattt.

## Status

✅ Accepted on 2020-08-04

## Consequences

### Positive

- All the math is already done and done right
- There is a test suite attached to this package
- Support for encoding and decoding is right built in

### Negative

- There is no conversation option from one currency to another which would require up-to-date conversion information, i.e. from the Internet
