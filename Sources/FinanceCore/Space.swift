public struct Credits {
    let amount : UInt
    
    init(_ initialAmount: UInt) {
        amount = initialAmount
    }
}

extension Credits : AdditiveArithmetic {
    public static func - (lhs: Credits, rhs: Credits) -> Credits {
        let result = lhs.amount - rhs.amount
        return Credits(result)
    }
    
    public static func + (lhs: Credits, rhs: Credits) -> Credits {
        return Credits(lhs.amount + rhs.amount)
    }
    
    public static var zero: Credits {
        return Credits(0)
    }
}

extension Credits: RoleplayingCurrency {
    var systemNames: [String] {
        ["Shadowrun", "Travellers (GURP)"]
    }
}
