public struct Lorakis {
    let solare: UInt
    let lunare: UInt8
    let telare: UInt8
    
    init(solare: UInt, lunare: UInt8, telare: UInt8) {
        var calculatedTelare = telare
        var calculatedLunare = lunare
        var calculatedSolare = solare
        
        if telare >= 100 {
            let upperAddition = telare / 100
            calculatedLunare += upperAddition
            calculatedTelare -= (upperAddition * 100)
        }
        
        if lunare >= 100 {
            let upperAddition = lunare / 100
            calculatedSolare += UInt(upperAddition)
            calculatedLunare -= (upperAddition * 100)
        }
        
        self.solare = calculatedSolare
        self.lunare = calculatedLunare
        self.telare = calculatedTelare
    }
    
    func normalize(_ value: UInt8) -> (UInt8, UInt8) {
        var upperValue:UInt8 = 0
        var resultValue = value
        
        if value >= 100 {
            let upperAddition = value / 100
            upperValue += upperAddition
            resultValue -= (upperAddition * 100)
        }
        
        return (upperValue, resultValue)
    }
}

extension Lorakis : AdditiveArithmetic {
    public static func - (lhs: Lorakis, rhs: Lorakis) -> Lorakis {
        return Lorakis(solare: lhs.solare + rhs.solare,
                       lunare: lhs.lunare + rhs.lunare,
                       telare: lhs.telare + rhs.lunare)
    }
    
    public static func + (lhs: Lorakis, rhs: Lorakis) -> Lorakis {
        var solare = lhs.solare + rhs.solare
        var lunare = lhs.lunare + rhs.lunare
        var telare = lhs.telare + rhs.telare
        
        if telare >= 100 {
            let upperAddition = telare / 100
            lunare += upperAddition
            telare -= (upperAddition * 100)
        }
        
        if lunare >= 100 {
            let upperAddition = lunare / 100
            solare += UInt(upperAddition)
            lunare -= (upperAddition * 100)
        }
        
        return Lorakis(solare: solare,
                       lunare: lunare,
                       telare: telare)
    }
    
    public static var zero: Lorakis {
        return Lorakis(solare: 0, lunare: 0, telare: 0)
    }
}

extension Lorakis : RoleplayingCurrency {
    var systemNames: [String] {
        ["Splittermond"]
    }
}
