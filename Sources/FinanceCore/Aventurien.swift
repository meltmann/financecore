public struct Mittelreichisch {
    let dukaten: UInt
    let silbertaler: UInt8
    let heller: UInt8
    let kreuzer: UInt8
}

extension Mittelreichisch : AdditiveArithmetic {
    public static func - (lhs: Mittelreichisch, rhs: Mittelreichisch) -> Mittelreichisch {
        return Mittelreichisch(dukaten: lhs.dukaten - rhs.dukaten,
                               silbertaler: lhs.silbertaler - rhs.silbertaler,
                               heller: lhs.heller - rhs.heller,
                               kreuzer: lhs.kreuzer - rhs.kreuzer)
    }
    
    public static func + (lhs: Mittelreichisch, rhs: Mittelreichisch) -> Mittelreichisch {
        return Mittelreichisch(dukaten: lhs.dukaten + rhs.dukaten,
                               silbertaler: lhs.silbertaler + rhs.silbertaler,
                               heller: lhs.heller + rhs.heller,
                               kreuzer: lhs.kreuzer + rhs.kreuzer)
    }
    
    public static var zero: Mittelreichisch {
        return Mittelreichisch(dukaten: 0, silbertaler: 0, heller: 0, kreuzer: 0)
    }
}

extension Mittelreichisch : RoleplayingCurrency {
    var systemNames: [String] {
        ["Das Schwarze Auge"]
    }
}
