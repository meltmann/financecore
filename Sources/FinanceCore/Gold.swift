public struct Imperial {
    let gold : UInt
    let silver: UInt
    let copper: UInt
}

extension Imperial : AdditiveArithmetic {
    public static func - (lhs: Imperial, rhs: Imperial) -> Imperial {
        return Imperial(gold: lhs.gold - rhs.gold, silver: lhs.silver - rhs.silver, copper: lhs.copper - rhs.copper)
    }
    
    public static func + (lhs: Imperial, rhs: Imperial) -> Imperial {
        return Imperial(gold: lhs.gold + rhs.gold, silver: lhs.silver + rhs.silver, copper: lhs.copper + rhs.copper)
    }
    
    public static var zero: Imperial {
        return Imperial(gold: 0, silver: 0, copper: 0)
    }
}

extension Imperial : RoleplayingCurrency {
    var systemNames: [String] {
        ["Warhammer Fantasy", "Warhammer: Age Of Sigmar"]
    }
}
