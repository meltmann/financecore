public struct IrdenEurope {
    let euro: UInt
    let cent: UInt8
}

extension IrdenEurope : AdditiveArithmetic {
    public static func - (lhs: IrdenEurope, rhs: IrdenEurope) -> IrdenEurope {
        return IrdenEurope(euro: lhs.euro - rhs.euro, cent: lhs.cent - rhs.cent)
    }
    
    public static func + (lhs: IrdenEurope, rhs: IrdenEurope) -> IrdenEurope {
        return IrdenEurope(euro: lhs.euro + rhs.euro, cent: lhs.cent + rhs.cent)
    }
    
    public static var zero: IrdenEurope {
        return IrdenEurope(euro: 0, cent: 0)
    }
}

extension IrdenEurope : RoleplayingCurrency {
    var systemNames: [String] {
        ["Chroniken der Engel", "Vampire", "Werewolf", "Magus"]
    }
}
