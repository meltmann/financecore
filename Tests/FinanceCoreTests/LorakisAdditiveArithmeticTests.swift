import XCTest
@testable import FinanceCore

final class LorakisAdditiveArithmeticTests: XCTestCase {

    func testZeroStack_hasNoEntries() {
        let money = Lorakis.zero
        
        checkEqualContents(money: money, expectation: (0,0,0))
    }
    
    func testInitialization_outsideLowLevelBounds_raisesUpperBounds() {
        let money = Lorakis(solare: 815, lunare: 123, telare: 255)
        
        checkEqualContents(money: money, expectation: (816,25,55))
    }
    
    func testAddition_innerLowLevelBounds_simplyAdds() {
        let money = Lorakis.zero + Lorakis(solare: 1, lunare: 2, telare: 4)

        checkEqualContents(money: money, expectation: (1,2,4))
    }
    
    func testAddition_outsideLowLevelBounds_raisesUpperBounds_telare() {
        var moneyPouchContent = Lorakis.zero
        moneyPouchContent += Lorakis(solare: 0, lunare: 0, telare: 99)
        
        moneyPouchContent += Lorakis(solare: 0, lunare: 0, telare: 1)
        checkEqualContents(money: moneyPouchContent, expectation: (0, 1, 0))
    }

    func testAddition_outsideLowLevelBounds_raisesUpperBounds_lunare() {
        var moneyPouchContent = Lorakis.zero
        moneyPouchContent += Lorakis(solare: 0, lunare: 99, telare: 0)
        
        moneyPouchContent += Lorakis(solare: 0, lunare: 1, telare: 0)
        checkEqualContents(money: moneyPouchContent, expectation: (1, 0, 0))
    }

    func testAddition_outsideLowLevelBounds_raisesUpperBounds() {
        var moneyPouchContent = Lorakis.zero
        moneyPouchContent += Lorakis(solare: 99, lunare: 99, telare: 99)
        
        moneyPouchContent += Lorakis(solare: 1, lunare: 1, telare: 1)
        checkEqualContents(money: moneyPouchContent, expectation: (101, 1, 0))
    }

    func testAddition_outsideLowLevelBounds_raisesMultipleUpperBounds() {
        var moneyPouchContent = Lorakis.zero
        moneyPouchContent += Lorakis(solare: 0, lunare: 99, telare: 99)
        
        moneyPouchContent += Lorakis(solare: 815, lunare: 123, telare: 255)
        checkEqualContents(money: moneyPouchContent, expectation: (817, 25, 54))
    }
    
    static var allTests = [
        ("ZeroProducesEmptyStack", testZeroStack_hasNoEntries),
        ("InitializationExceedingBounds", testInitialization_outsideLowLevelBounds_raisesUpperBounds),
        ("AdditionWithinSubTypeBoundaries", testAddition_innerLowLevelBounds_simplyAdds),
        ("AdditionExceedingTelareTypeBoundaries", testAddition_outsideLowLevelBounds_raisesUpperBounds_telare),
        ("AdditionExceedingLunareTypeBoundaries", testAddition_outsideLowLevelBounds_raisesUpperBounds_lunare),
        ("AdditionExceedingTypeBoundaries", testAddition_outsideLowLevelBounds_raisesUpperBounds),
        ("AdditionExceedingTypeBoundariesMultipleTimes", testAddition_outsideLowLevelBounds_raisesMultipleUpperBounds),
    ]
    
    
    func checkEqualContents(money: Lorakis, expectation: (UInt, UInt8, UInt8)) {
        XCTAssertEqual(money.solare, expectation.0)
        XCTAssertEqual(money.lunare, expectation.1)
        XCTAssertEqual(money.telare, expectation.2)
    }
}
