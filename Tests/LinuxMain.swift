import XCTest

import FinanceCoreTests

var tests = [XCTestCaseEntry]()
tests += FinanceCoreTests.allTests()
XCTMain(tests)
